package com.example.chatrine.portalberita.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InitLibrary {
    public static String URL_API = "http://192.168.20.85:8888/portal_berita/";
    public static String URL_API_GAMBAR = "http://192.168.20.85:8888/portal_berita/images/";

    public static Retrofit setInit(){
        return new Retrofit.Builder().baseUrl(URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiServices getInstances(){
        return setInit().create(ApiServices.class);
    }
}
