package com.example.chatrine.portalberita.network;

import com.example.chatrine.portalberita.network.response.ResponseBerita;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiServices {
    // Buat request
    @GET("tampil_berita.php")
    Call<ResponseBerita> request_berita();
}
