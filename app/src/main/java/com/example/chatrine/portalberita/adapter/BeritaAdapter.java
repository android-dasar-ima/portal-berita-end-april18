package com.example.chatrine.portalberita.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.chatrine.portalberita.R;
import com.example.chatrine.portalberita.network.InitLibrary;
import com.example.chatrine.portalberita.network.response.BeritaItem;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BeritaAdapter extends RecyclerView.Adapter<BeritaAdapter.ViewHolder> {
    Context context;
    List<BeritaItem> dataBerita; // data berita dari response
    public BeritaAdapter(Context context, List<BeritaItem> dataBerita) {
        this.context = context;
        this.dataBerita = dataBerita;
    }

    @NonNull
    @Override
    public BeritaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Buat view
        View view = LayoutInflater.from(context).inflate(R.layout.item_berita, parent, false);
        // Masukan View kedalam Holder
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull BeritaAdapter.ViewHolder holder, int position) {
        // disini bagian diman kita bisa memanipulasi widget
        holder.tvJudulBerita.setText(dataBerita.get(position).getJudulBerita());
        holder.tvTanggalBerita.setText(dataBerita.get(position).getTanggalPosting());
        holder.tvPenulisBerita.setText("Oleh : " + dataBerita.get(position).getPenulis());
        // Buat URL gambar
        String url_gambar = InitLibrary.URL_API_GAMBAR + "im" + dataBerita.get(position).getFoto();
        // Picasso me load gambar dari URL ke widget image view
        Picasso.with(context).load(url_gambar).into(holder.ivGambarBerita);
    }

    @Override
    public int getItemCount() {
        return dataBerita.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // Deklarasi Widget
        ImageView ivGambarBerita;
        TextView tvJudulBerita, tvTanggalBerita, tvPenulisBerita;
        public ViewHolder(View itemView) {
            super(itemView);
            // Inisialisasi
            ivGambarBerita = itemView.findViewById(R.id.ivGambarBerita);
            tvJudulBerita = itemView.findViewById(R.id.tvJudulBerita);
            tvPenulisBerita = itemView.findViewById(R.id.tvPenulis);
            tvTanggalBerita = itemView.findViewById(R.id.tvTanggal);
        }
    }
}
