package com.example.chatrine.portalberita;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.chatrine.portalberita.adapter.BeritaAdapter;
import com.example.chatrine.portalberita.network.ApiServices;
import com.example.chatrine.portalberita.network.InitLibrary;
import com.example.chatrine.portalberita.network.response.BeritaItem;
import com.example.chatrine.portalberita.network.response.ResponseBerita;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.rvListBerita)
    RecyclerView rvListBerita;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Recycler view membutuhkan Layout manager
        rvListBerita.setLayoutManager(new LinearLayoutManager(this));

        // Eksekusi Method
        getBerita();
    }
    public void getBerita(){
        // Siapkan retrofit
        ApiServices api = InitLibrary.getInstances();
        // Siapkan request
        Call<ResponseBerita> call = api.request_berita();
        // Kirim request
        call.enqueue(new Callback<ResponseBerita>() {
            @Override
            public void onResponse(Call<ResponseBerita> call, Response<ResponseBerita> response) {
                // pastikan response sukses
                if (response.isSuccessful()){
                    // perintah ketika response berhasil
                    Log.d("response berita", response.body().toString());
                    // tampung
                    boolean dataAda = response.body().isStatus();
                    List<BeritaItem> dataBerita = response.body().getBerita();
                    if (dataAda){
                        // Panggil adapter
                        BeritaAdapter adapter = new BeritaAdapter(MainActivity.this, dataBerita);
                        // set adapter ke widget
                        rvListBerita.setAdapter(adapter);
                    } else {
                        // tampilkan toast
                        Toast.makeText(MainActivity.this, "Berita kosong", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    // tampilkan Toast
                    Toast.makeText(MainActivity.this, "Terjadi Kesalahan : " + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBerita> call, Throwable t) {
                // cetak error di Log
                t.printStackTrace();
            }
        });
    }
}
